﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickScript : MonoBehaviour {

    Rigidbody m_rigid;
    public Transform mario;
   
    // Use this for initialization
    void Start () {
        m_rigid = mario.gameObject.GetComponent<Rigidbody>();

    }
	
	// Update is called once per frame
	void FixedUpdate () {
		if(Input.GetMouseButton(0))
        {
            HitObject();
            // mario.position = 
            // m_rigid.velocity = Vector3.zero + new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0.0f) * 15.0f;
        }
	}

    void HitObject()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction*10.0f, new Color(1,1,0),1.0f);
        if (Physics.Raycast(ray, out hit))
        {
            Rigidbody r = hit.collider.GetComponent<Rigidbody>();

            if (r != null)
            {
                r.AddExplosionForce(3000.0f, hit.point, 1.0f);
                // r.AddForceAtPosition(hit.point, hit.normal* 3000.0f);
                ///hit.collider.gameObject.GetComponent<MeshRenderer>().material.color = Color.blue;
                ///hit.collider.gameObject.transform.localScale = hit.collider.gameObject.transform.localScale * .9f;
                /// hit.collider.gameObject.transform.Rotate(Vector3.up, 15.0f);

            }
        }

        //Vector3 mousePos = Input.mousePosition + new Vector3(0,0,10.0f);
       // return Camera.main.ScreenToWorldPoint(mousePos);
    }

}
