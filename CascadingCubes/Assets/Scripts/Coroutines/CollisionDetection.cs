﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class CollisionDetection : MonoBehaviour {

    public int m_id = -1;

    public Rigidbody m_rigidbody;

    void Start()
    {
        m_rigidbody = GetComponent<Rigidbody>();
    }


    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Mario"))
        {
            SendMessageUpwards("StartCascade", m_id);
        }
    }

}
