﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickScript : MonoBehaviour {

  
   
    // Use this for initialization
    void Start () {
       

    }
	
	// Update is called once per frame
	void FixedUpdate () {
		if(Input.GetMouseButtonDown(0))
        {
            HitObject();
           
        }
	}

    void HitObject()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction*10.0f, new Color(1,1,0),1.0f);
        if (Physics.Raycast(ray, out hit))
        {
            Rigidbody r = hit.collider.GetComponent<Rigidbody>();

            if (r != null)
            {
                r.AddExplosionForce(8000.0f, hit.point, 1.0f);
                hit.collider.gameObject.GetComponent<MeshRenderer>().material.color = Color.red;
                ///hit.collider.gameObject.transform.localScale = hit.collider.gameObject.transform.localScale * .9f;
                /// hit.collider.gameObject.transform.Rotate(Vector3.up, 15.0f);

            }
        }

        //Vector3 mousePos = Input.mousePosition + new Vector3(0,0,10.0f);
       // return Camera.main.ScreenToWorldPoint(mousePos);
    }

}
