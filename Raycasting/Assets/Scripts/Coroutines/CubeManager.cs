﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeManager : MonoBehaviour {

    public int arraySize = 10;

    public CollisionDetection[] blocks;

    public GameObject preFab;

    public bool isTriggered = false;

    public float waitTime = 0.25f;

	// Use this for initialization
	void Start () {
        blocks = new CollisionDetection[arraySize];
        int startX = ((int)transform.position.x) - arraySize / 2;

        for (int j = 0; j < arraySize; j++)
        {
            GameObject instance = Instantiate(preFab,
                new Vector3(startX++, transform.position.y, transform.position.z),Quaternion.identity) as GameObject;
            CollisionDetection col = instance.GetComponent<CollisionDetection>();
            instance.transform.parent = transform;
            col.m_id = j;
            blocks[j] = col;

        }
        	
	}

    public void StartCascade(int id)
    {
        if(!isTriggered)
        {
            StartCoroutine(CascadeCoroutine(id));
            isTriggered = true;
        }

    }

    public void Reset()
    {
        for (int j = 0; j < arraySize; j++)
        {
            StopAllCoroutines();
            blocks[j].m_rigidbody.isKinematic = true;
            blocks[j].transform.position = new Vector3(blocks[j].transform.position.x, transform.position.y, blocks[j].transform.position.z);
            isTriggered = false;
        }
    }


    public IEnumerator CascadeCoroutine(int id)
    {
        int startBlock = id;

        int leftBlock = id - 1;
        int rightBlock = id + 1;

        blocks[id].m_rigidbody.isKinematic = false;

        while (true)
        {
            yield return new WaitForSeconds(waitTime);

            if (leftBlock < 0 && rightBlock >= arraySize) yield break;

            if (rightBlock < arraySize)
            {
                blocks[rightBlock++].m_rigidbody.isKinematic = false;
            }

            if (leftBlock >= 0)
            {
                blocks[leftBlock--].m_rigidbody.isKinematic = false;
            }

        }
    }


}
