﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterTurning : MonoBehaviour {
    [SerializeField]
    private float sensitivity = 10.0f;

    private float rotationAngle = 0.0f;

    // Use this for initialization
    void Start()
    {

    }

    void FixedUpdate()
    {
        if (Input.GetMouseButton(1))
        {
            rotationAngle += Input.GetAxis("Mouse X") * sensitivity;
            if (rotationAngle >= 360.0f) rotationAngle -= 360.0f;
            else if (rotationAngle <= 0.0f) rotationAngle += 360.0f;
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, rotationAngle, transform.rotation.eulerAngles.z);
        }


    }
}
