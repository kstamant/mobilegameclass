﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CharacterMovement : MonoBehaviour {

    [SerializeField]
    private float amt = 10.0f;
    [SerializeField]
    private GameObject m_groundCheck;
    [SerializeField]
    private float jumpForce = 1000.0f;

    [SerializeField]
    private int numJumps = 1;

    private int jumpsLeft;


    Rigidbody m_rigidBody;

    private Vector3 halfCasts = new Vector3(.1f,.5f,.1f);
    int layerMask;
    bool onGround = false;
    void Start()
    {
        jumpsLeft = numJumps;
        m_rigidBody = GetComponent<Rigidbody>();
        layerMask = LayerMask.NameToLayer("Ground");
    }

    void FixedUpdate()
    {

        onGround = false;
        if (Input.GetKey(KeyCode.W)) m_rigidBody.AddForce(transform.forward * amt);
        if (Input.GetKey(KeyCode.S)) m_rigidBody.AddForce(-transform.forward * amt);
        if (Input.GetKey(KeyCode.D)) m_rigidBody.AddForce(transform.right * amt);
        if (Input.GetKey(KeyCode.A)) m_rigidBody.AddForce(-transform.right * amt);


        Collider[] colliderList = Physics.OverlapBox(m_groundCheck.transform.position, halfCasts,Quaternion.identity);
        foreach(Collider c in colliderList)
        {
            if(!c.gameObject.Equals(gameObject))
            {
                jumpsLeft = numJumps;
                onGround = true;
                break;
            }
        }

        if (onGround && Input.GetKeyDown(KeyCode.Space))
        {
           
            m_rigidBody.AddForce(transform.up * jumpForce);
        }
        //else if()
        //{
        //    jumpsLeft--;
        //    m_rigidBody.AddForce(transform.up * jumpForce);
        //}




    }

   // private void DoGroundCheck()

	

}
