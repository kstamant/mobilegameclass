﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateOverTime : MonoBehaviour {

    public float amt = 15.0f;
    public Vector3 axis;
    // Update is called once per frame
    void Update () {
        transform.Rotate(axis, amt * Time.deltaTime);
	}
}
