﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchScene : MonoBehaviour {

    public string sceneToLoad = "Blood";
    public float time = 5.0f;
    // Use this for initialization
    void Start () {
        Invoke("DoSceneLoad", time);
	}
	
	private void DoSceneLoad()
    {
        SceneManager.LoadScene(sceneToLoad);
    }
}
