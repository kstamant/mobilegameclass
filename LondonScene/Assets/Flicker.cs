﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flicker : MonoBehaviour {


    public struct FlickerData
    {
        public Vector3 data;
        public bool isFlickering;
        public bool alreadyFlickered;
    }

    public float duration = 5.0f;

    public FlickerData[] flickers;
    public Vector3[] flickData;
    public int flickerLength = 0;
    public int flickerStartTime = 1;
    public int flickerAmount = 2;

    private float m_originalIntensity;
    private Light m_light;
    private int flickerCount;
    private float globalTimer = 0.0f;

    private float[] flickerTimers;
    public bool useSound = false;
    public AudioSource sound;
    public float soundCutoff = 3.0f;
    private float cutoffTimer = 0.0f;


    // Use this for initialization
    void Start () {
        flickerCount = flickData.Length;
        flickers = new FlickerData[flickerCount];
        flickerTimers = new float[flickerCount];
        for (int j = 0; j < flickerCount; ++j)
        {
            flickers[j].data = flickData[j];
            flickers[j].isFlickering = false;
            flickers[j].alreadyFlickered = false;
        }
        m_light = GetComponent<Light>();
        m_originalIntensity = m_light.intensity;
    }
	
    void Reset()
    {
        for (int j = 0; j < flickerCount; ++j)
        {
            flickers[j].isFlickering = false;
            flickers[j].alreadyFlickered = false;
            flickerTimers[j] = 0.0f;
        }
        globalTimer = 0.0f;
       // m_light.intensity = m_originalIntensity;
    }

	// Update is called once per frame
	void Update () {

        globalTimer += Time.deltaTime;

        if(globalTimer >= duration)
        {
            Reset();
        }
        if (useSound && sound.isPlaying) cutoffTimer += Time.deltaTime;
        if (cutoffTimer >= soundCutoff)
        {
            cutoffTimer = 0.0f;
            sound.Stop();

        }

        for (int j = 0; j < flickerCount; ++j)
        {
            if (!flickers[j].alreadyFlickered)
            {
                if (flickers[j].isFlickering)
                {
                    flickerTimers[j] += Time.deltaTime;

                    if (flickers[j].data[flickerLength] <= flickerTimers[j])
                    {
                        flickers[j].isFlickering = false;
                        flickers[j].alreadyFlickered = true;
                        if (useSound && !sound.isPlaying) sound.Play();
                        //m_light.intensity -= flickers[j].data[flickerAmount];
                    }



                }
                else if (flickers[j].data[flickerStartTime] <= globalTimer && !flickers[j].isFlickering)
                {
                    m_light.intensity += flickers[j].data[flickerAmount];
                    flickers[j].isFlickering = true;
                }
            }

        }


    }
}
