﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunTimerStart : MonoBehaviour {

	// Use this for initialization
	void Start () {
        PlayerPrefs.SetFloat("StartTime", Time.unscaledTime);
	}
	
}
