﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileManager : MonoBehaviour {

    public GameObject player;

    public bool fireAtPlayer = true;
    public bool canFire = true;

    public GameObject missilePrefab;

    public int numMissles = 20;

    public float spread = 1.0f;
    public float lag = 1.0f;

    public float delay = 1.0f;
    public float speed = 1.0f;
    public float speedScale = 1.0f;
    GameObject[] missiles;

    // Use this for initialization
    void Start()
    {

        missiles = new GameObject[numMissles];

        for (int j = 0; j < numMissles; ++j)
        {
            missiles[j] = Instantiate(missilePrefab, transform) as GameObject;
            missiles[j].GetComponent<WormMissile>().speed = speed + speedScale * j;

            missiles[j].SetActive(false);
        }

    }
    public void SetPlayer(GameObject obj)
    {
        player = obj;
    }
    public IEnumerator MissileCoroutine()
    {
        Vector3 dir = ((player.transform.position + new Vector3(0, Random.Range(-spread, spread), 0)) - transform.position).normalized;

        for (int j = 0; j < numMissles; ++j)
        {
            missiles[j].transform.SetParent(transform);
            missiles[j].transform.localPosition = Vector2.zero;
            missiles[j].transform.SetParent(null);
            missiles[j].SetActive(true);
            Vector3 newDir;
            if (fireAtPlayer)
            {

                newDir = ((player.transform.position + new Vector3(0, Random.Range(-spread, spread), 0)) - missiles[j].transform.position).normalized;
                dir = Vector3.Lerp(dir, newDir, lag).normalized;
            }
            else {
                newDir = new Vector3(Random.Range(0.01f,1.0f), Random.Range(0.01f, 1.0f), Random.Range(0.01f, 1.0f)).normalized;
            }

            missiles[j].SendMessage("Fire", dir);
            if (j < numMissles - 1)
            {
                yield return new WaitForSeconds(delay);
            }
            else
            {
                yield return new WaitForSeconds(delay / 2.0f);
            }
        }

        yield break;
    }
    public void SetCanFire(bool canfire)
    {
        canFire = canfire;
    }
    public void FireMissiles()
    {
        if (canFire)
        {
            StartCoroutine(MissileCoroutine());
        }
    }

}
