﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpBarManager : MonoBehaviour {

    public enum HPstate { HEALTHY, WOUNDED, PERIL };
    public HPstate state = HPstate.HEALTHY;

    public EnemyHealth m_health;
    public SpriteRenderer hp_renderer;


    public Sprite healthy;
    public Sprite wounded;
    public Sprite peril;

    public float width = 60.0f;
    public float height = 10.0f;

    public void UpdateHP()
    {
        //bar.rect.width = // new Vector3(30.0f - (maxHealth - health) / (maxHealth / 30.0f), 2.6625f, 2.6625f);

        transform.localScale = new Vector3(width * (m_health.m_hp / m_health.m_maxHP), 0.6f,1.0f);
        //bar.rect.width = width;
        if (m_health.m_hp / m_health.m_maxHP <= 0.333f)
        {
            state = HPstate.PERIL;
            hp_renderer.sprite = peril;

        }
        else if (m_health.m_hp / m_health.m_maxHP <= 0.666f)
        {
            state = HPstate.WOUNDED;
            hp_renderer.sprite = wounded;

        }

    }

    public void DisplayBar(bool on)
    {
        hp_renderer.gameObject.SetActive(on);
    }


    void Start()
    {
        hp_renderer.sprite = healthy;
        transform.localScale = new Vector3(width * (m_health.m_hp / m_health.m_maxHP), 0.6f, 1.0f);

        //hp_renderer.gameObject.SetActive(false);

        state = HPstate.HEALTHY;
    }
    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.identity;

    }
}
