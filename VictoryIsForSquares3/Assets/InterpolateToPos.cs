﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterpolateToPos : MonoBehaviour {
    public Transform toPos;
    Vector2 original;

    public float dur = 6.0f;
    public float timer = 0.0f;

    public float pause = 6.0f;
    public bool floatingDown = false;
    public bool floatingUp = false;
    public bool waiting = false;


    public void Trigger()
    {
        timer = 0.0f;

        floatingDown = true;
        original = transform.position;
        StartCoroutine(ReturnCoroutine());
    }

    public IEnumerator ReturnCoroutine()
    {
        yield return new WaitForSeconds(dur);
        timer = 0.0f;
        floatingDown = false;
        waiting = true;

        yield return new WaitForSeconds(pause);
        timer = 0.0f;
        waiting = false;
        floatingUp = true;
        yield return new WaitForSeconds(dur);
        floatingUp = false;

        yield break;
    }



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;

        if (floatingDown)
        {
            transform.position = Vector2.Lerp(original, toPos.position, timer / dur);
        }
        else if(floatingUp)
        {
            transform.position = Vector2.Lerp(toPos.position, original, timer / dur);
        }
	}
}
