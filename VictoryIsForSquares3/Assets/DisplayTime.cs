﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DisplayTime : MonoBehaviour {

    public Text timer;
	// Use this for initialization
	void Start () {
        float startTime = PlayerPrefs.GetFloat("StartTime", 0);
        float curTime = Time.unscaledTime;

        float time = curTime - startTime;

        string timeStr = string.Format("{0}:{1:00}.{2:00}", (int)time / 60, (int)time % 60, (int)(Mathf.Repeat(time, 1.0f) * 1000));


        timer.text += timeStr;

    }


}
