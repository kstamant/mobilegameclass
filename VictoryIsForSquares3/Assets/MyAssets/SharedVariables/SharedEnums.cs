﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.MyAssets.SharedVariables
{

    [System.Serializable]
    public enum AttackState
    {
        WAITING,
        ATTACKING,
        SLAMMING,
        RETURNING,
        RETURN_SLAMMING,
        DYING,
        FIRING,

    }

    [System.Serializable]
    public enum WormState
    {
        HIDING,
        DEAD,
        WAITING,
        ATTACKING,
        DYING,
        DAMAGED,
        FIRING,
    }




}
