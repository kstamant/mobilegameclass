﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOverTime : MonoBehaviour {

	// Use this for initialization

    float m_amt = 0.0f;
    float m_dur = 0.0f;
    bool started = false;
    float timer = 0.0f;
    float m_interval = 1.0f;
    bool justHit = false;

    public void ApplyDamage(float amt, float dur, float interval)
    {
        if (!started)
        {
            m_amt = amt;
            m_dur = dur;
            started = true;
        }
    }

    // Update is called once per frame
    void Update () {
		if(started)
        {
            timer += Time.deltaTime;

            if(!justHit && Mathf.Repeat(m_interval, timer) < .2f)
            {
                SendMessage("Damage", m_amt);

                justHit = true;
            }
            else
            {
                justHit = false;
            }

        }
	}
}
