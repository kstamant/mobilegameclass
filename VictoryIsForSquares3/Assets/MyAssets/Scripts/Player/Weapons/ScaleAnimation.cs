﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class ScaleAnimation : MonoBehaviour {
    public Animator a;
    public string triggerName;
    //public GameObject tears;

    void Start()
    {
        a = GetComponent<Animator>();
    }

    public void SetScale(float scale)
    {
        //if (scale == 1.0f)
        //{
        //    GameObject temp = Instantiate(tears, transform.position, transform.rotation) as GameObject;
        //    temp.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(0, 10.0f));
        //}
        transform.localScale = new Vector3(scale, scale, 1.0f);
        a.ResetTrigger(triggerName);
    }

}
