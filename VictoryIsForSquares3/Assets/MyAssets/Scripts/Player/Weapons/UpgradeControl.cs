﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.MyAssets.Delegates;

public class UpgradeControl : MonoBehaviour {

    public GameObject upgrade;

    public float delay = 3.0f;
    private float time = 0.0f;
    private bool onCoolDown = false;
    private bool equipped = false;

    event ObjectOperationDelegate equipDelegate;
    public void Init(ObjectOperationDelegate e)
    {
        equipDelegate += e;
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (!onCoolDown)
            {
                onCoolDown = true;
                equipped = !equipped;
                equipDelegate(upgrade, equipped);
            }


        }


    }
	
	// Update is called once per frame
	void Update () {
		if (onCoolDown)
        {
            time += Time.deltaTime;
            if(time >= delay)
            {
                time = 0.0f;
                onCoolDown = false;
            }

        }
	}
}
