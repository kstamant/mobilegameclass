﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackInfo : MonoBehaviour {
    public string attackName = "Generic Attack";
    public float damage = 1.0f;
    public float damageOverTime = 0.0f;
    public float damageOverTimeDuration = 0.0f;
    
}
