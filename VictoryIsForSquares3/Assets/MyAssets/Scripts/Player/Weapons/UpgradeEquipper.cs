﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.MyAssets.Delegates;

public struct GameObjData
{
    public GameObject obj;
    public bool opt;
}


public class UpgradeEquipper : MonoBehaviour {

    public GameObject player;


    public GameObject[] upgrades;

    // Use this for initialization
    void Start () {
       // if (player == null) Application.Quit();

        foreach(GameObject obj in upgrades)
        {
            UpgradeControl u = obj.GetComponent<UpgradeControl>();
            
            u.Init(EquipUpgrade);

        }

    }


    public void EquipUpgrade(GameObject upgrade, bool optIn)
    {
        upgrade.SendMessage("Equip", new GameObjData { obj = player, opt = optIn });
    }


	// Update is called once per frame
	void Update () {
		
	}

}
