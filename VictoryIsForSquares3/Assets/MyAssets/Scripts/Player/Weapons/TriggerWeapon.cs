﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityStandardAssets.CrossPlatformInput;
using Assets.MyAssets.SharedVariables;
public class TriggerWeapon : MonoBehaviour {

    public UnityEvent events;

    public PlayerControl character;
    public Rigidbody2D charRigidbody;
    public float forceUp, forceForwards;
    public AudioSource attackSound;
    float time = 0;
    public float waitTime = .5f;
    public bool ready = true;
    public bool equipped = false;
    public SpikeControl spikes;

    public void Equip(GameObjData player)
    {

        if (player.opt)
        {
            spikes.Equip(player);
            if (!equipped)
            {
                charRigidbody = player.obj.GetComponent<Rigidbody2D>();
                character = player.obj.GetComponent<PlayerControl>();
                if (!(charRigidbody == null || character == null))
                {

                    equipped = true;
                    transform.SetParent(player.obj.transform);

                    transform.localPosition = Vector3.zero;

                    Transform sprite = player.obj.transform.FindChild(SharedStrings.playerSprite);
                    spikes.playerSprite = sprite.gameObject.GetComponent<SpriteRenderer>();
                    sprite.parent = spikes.transform;

                    spikes.playerRigidbody = player.obj.GetComponent<Rigidbody2D>();

                }
                else
                {
                    Debug.Log(("Equipping " + gameObject.name + " to player failed"));
                }
            }
        }
        //else
        //{
        //    if (equipped)
        //    {
        //        equipped = false;
        //
        //        charRigidbody = null;
        //        character = null;
        //        transform.SetParent(null);
        //        transform.position = Vector3.zero;
        //        Transform sprite = spikes.transform.FindChild(SharedStrings.playerSprite);
        //        sprite.parent = player.obj.transform;
        //        sprite.localPosition = new Vector3(-0.06f, 0, 0);
        //        spikes.playerSprite = null;
        //
        //        spikes.playerRigidbody = null;
        //    }
        //}

    }

   

    //x offset: -0.06
    void Update()
    {

        if(equipped && ready && !character.m_onGround && CrossPlatformInputManager.GetButtonDown("Jump"))
        {
            // SendMessageUpwards("SmallJump");
            // if(character.m_FacingRight)
            // {
            //     charRigidbody.AddForce(new Vector2(forceForwards, forceUp));
            // }
            // else
            // {
            //     charRigidbody.AddForce(new Vector2(-forceForwards, forceUp));
            //
            // }
            attackSound.Play();

            events.Invoke();
            ready = false;
        }
        else if(!ready)
        {
            time += Time.deltaTime;
            if(time >= waitTime)
            {
                ready = true;
                time = 0.0f;
                
            }
        }
    }

}
