﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeControl : MonoBehaviour {

    public string attackAnim;
    public string attackAnim2;
    public Animator[] animators;
    public Animator animator;
    public Animator playerAnimator;
    public Animation spinAnim;
    public SpriteRenderer playerSprite;
    public ScaleAnimation[] scales;
    public Rigidbody2D playerRigidbody;
    public Vector2 attackForce;
    private Vector2 negativeAttackForce;
    public PlayerControl player;


    // Use this for initialization
    void Start () {
        animators = GetComponentsInChildren<Animator>();
        scales = GetComponentsInChildren<ScaleAnimation>();
        animator = GetComponent<Animator>();
        spinAnim = GetComponent<Animation>();
        negativeAttackForce = new Vector2(-attackForce.x, attackForce.y);
    }

    public void Equip(GameObjData player)
    {
        if (this.player == null)
        {
            this.player = player.obj.GetComponent<PlayerControl>();
        }
    }


    public void SetDir(bool right)
    {
       
    }
	
    //public void PlayAnimation(string animation)
    //{

        

    //    foreach (Animator a in animators)
    //    {
    //        if (!a.Equals(animator)) a.SendMessage("Play", animation);
    //    }

    //}

    public void SetTriggers(string trigger)
    {
        bool right = playerSprite.flipX;

        if(right)
        {
            playerRigidbody.AddForce(attackForce);
            animator.Play(attackAnim);

        }
        else
        {
            playerRigidbody.AddForce(negativeAttackForce);
            animator.Play(attackAnim2);

        }



        foreach (ScaleAnimation s in scales)
        {
            s.SetScale(1.0f);
        }
        foreach (Animator a in animators)
        {
            if (!a.Equals(animator) && !a.Equals(playerAnimator)) a.SetTrigger(trigger);
        }
    }

}
