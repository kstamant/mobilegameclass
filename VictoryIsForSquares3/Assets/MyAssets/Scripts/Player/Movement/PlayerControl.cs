﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;


public class PlayerControl : MonoBehaviour {

    public float m_horizontalForce = 10f;
    public float m_maxHorizontalSpeed = 200f;
    public float m_JumpForce = 400f;
    public bool m_canControlInAir = true;
    private float oldMaxHorizontalSpeed;

    //private int m_inputLagCounter = 0;
    //public int m_inputLagWait = 2;
    //private bool[] inputs;

    public float m_smallJumpModifier = .5f;

  
    public LayerMask m_whatIsGround; //Lessens the amount of physics checks

    [SerializeField]
    private AudioSource m_jumpSound;

    public Transform m_groundCheck;
    public Transform m_ceilingCheck;

    public float m_groundContactDist = .16f;
    public Vector2 m_groundContactBox;
    public float m_ceilingContactDist = .01f;
    public Vector2 m_ceilingContactBox;

    public bool m_onGround = true;


    public Animator m_animator; //Player animator

    public Rigidbody2D m_Rigidbody2D;

    public SpriteRenderer m_playerSprite;

    public bool m_facingRight = false;  // For determining which way the player is currently facing.


    public bool m_alive = true;
    public bool m_canMove = true;

    public bool m_jump = false;

    //For cutscenes
    public bool m_movementEnabled = true;

    public GameObject[] m_collisions;
    public int m_currentCollision = 0;
    public int m_maxColliders = 25;


    public bool stillHolding = false;
    public bool alreadyJumpedOnPress = false;

    void Start()
    {
        m_collisions = new GameObject[m_maxColliders];
        oldMaxHorizontalSpeed = m_maxHorizontalSpeed;
        //inputs = new bool[m_inputLagWait];
    }

    public void ResetMaxSpeed()
    {
        m_maxHorizontalSpeed = oldMaxHorizontalSpeed;
    }
    public void SetMaxSpeed(float speed)
    {
        m_maxHorizontalSpeed = speed;
    }

    //// Update is called once per frame
    //private void Update()
    //{
    //    if (m_movementEnabled)
    //    {
    //        if (!m_jump)
    //        {
    //            m_jump = CrossPlatformInputManager.GetButtonDown("Jump");
    //            m_inAir = m_jump;
    //        }
    //    }
    //
    //}

    // void OnTriggerEnter2D(Collider2D other)
    // {
    //
    //     if(other.CompareTag("Debug"))
    //     {
    //         int a = 0;
    //         a++; 
    //     }
    //     if (((1 << other.gameObject.layer) & m_whatIsGround.value) > 0)
    //     {
    //         //m_collisions[m_currentCollision] = coll.collider.gameObject;
    //
    //         m_currentCollision++;
    //     }
    // }
    // void OnTriggerExit2D(Collider2D other)
    // {
    //     if (( (1 << other.gameObject.layer) & m_whatIsGround.value) > 0)
    //     {
    //         //m_onGround = true;
    //         m_currentCollision--;
    //         //if (m_currentCollision < 0) m_currentCollision = 0;
    //     }
    // }

    private void FixedUpdate()
    {
        if (m_alive)
        {
            m_onGround = false;
            //m_onGround = (m_currentCollision > 0) ;
            Collider2D[] colliders = Physics2D.OverlapBoxAll(m_groundCheck.position, m_groundContactBox, 0.0f, m_whatIsGround);
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject != gameObject)
                {
                    m_onGround = true;
                    break;
                }
            }

            Collider2D[] collidersCiel = Physics2D.OverlapBoxAll(m_ceilingCheck.position, m_ceilingContactBox, 0.0f, m_whatIsGround);
            for (int i = 0; i < collidersCiel.Length; i++)
            {
                if (collidersCiel[i].gameObject != gameObject)
                {
                    m_onGround = false;
                    break;
                }
            }

            if (m_movementEnabled)
            {
                float h = CrossPlatformInputManager.GetAxis("Horizontal");

                bool jumpDown =  CrossPlatformInputManager.GetButton("Jump");
                if (stillHolding && !jumpDown)
                {
                    alreadyJumpedOnPress = false;
                }
                stillHolding = jumpDown;

                bool willJump = false;


               //for (int j = 0; j < m_inputLagWait - 1; j++)
               //{
               //    inputs[j] = inputs[j + 1];
               //}
               //inputs[m_inputLagWait - 1] = jumpDown;


                if (m_onGround)
                {

                    willJump = jumpDown;
                    //if (!willJump)
                    //{
                    //    for (int j = 0; j < m_inputLagWait - 1; j++)
                    //    {
                    //        if (inputs[j])
                    //        {
                    //            willJump = true;
                    //            break;
                    //        }
                    //    }
                    //}

                }
              m_jump = !alreadyJumpedOnPress && willJump;
              Move(h);
            }
            m_jump = false;

        }
    }

    void Kill()
    {
        m_alive = false;
    }


    public void Move(float horizontalDelta)
    {  
       if (m_onGround || m_canControlInAir)
       {    
            m_Rigidbody2D.AddForce(new Vector2(horizontalDelta * m_horizontalForce * Time.deltaTime, 0));
            Vector2 temp = new Vector2(m_Rigidbody2D.velocity.x, 0.0f);
            if (temp.magnitude > m_maxHorizontalSpeed)
            {
                m_Rigidbody2D.velocity = new Vector2(temp.normalized.x * m_maxHorizontalSpeed, m_Rigidbody2D.velocity.y);
            }

                if (horizontalDelta > 0 && !m_facingRight)
                {
                    Flip();
                }
                else if (horizontalDelta < 0 && m_facingRight)
                {
                    Flip();
                }
       }

       if(m_onGround && m_jump)
       {
          alreadyJumpedOnPress = true;
          Jump();
          //m_jumpSound.Play();
          //m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
       }
    }

    //For sending messages 
    public void Jump()
    {
        m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
        m_jumpSound.Play();
    }

    public void AddPushX(float x)
    {
        m_Rigidbody2D.AddForce(new Vector2(x,0));
        m_jumpSound.Play();
    }

    public void AddPushY(float y)
    {
        m_Rigidbody2D.AddForce(new Vector2(0, y));
        m_jumpSound.Play();
    }
    public void RemoveControl(float time)
    {

        StartCoroutine(PauseControl(time));
    }

    public IEnumerator PauseControl(float time)
    {

        m_movementEnabled = false;
        yield return new WaitForSeconds(time);

        m_movementEnabled = true;

        yield break;
    }

    //For sending messages, used for bouncing
    public void SmallJump()
    {
        m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce * m_smallJumpModifier));
        m_jumpSound.Play();
    }

    private void Flip()
    {
        m_facingRight = !m_facingRight;;
        m_playerSprite.flipX = !m_facingRight;

    }


}
