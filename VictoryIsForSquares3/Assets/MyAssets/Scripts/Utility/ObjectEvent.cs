﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
public class ObjectEvent : MonoBehaviour {
    private bool m_enabled = true;

    [SerializeField]
    private GameObject m_object;

    [SerializeField]
    private string m_eventMethod = "";
    [SerializeField]
    private string m_input = "Submit";
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (m_enabled)
        {
            if (CrossPlatformInputManager.GetButtonDown(m_input))
            {
                m_object.SendMessage(m_eventMethod, SendMessageOptions.DontRequireReceiver);
            }
        }
	}
}
