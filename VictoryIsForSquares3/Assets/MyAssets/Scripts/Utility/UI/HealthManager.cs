﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HealthManager : MonoBehaviour {

    public int m_maxContainers = 20;
    public Canvas m_canvas;
    public GameObject m_heartPrefab;

    HeartContainer[] m_containers;

    public int m_maxHP = 1;
    public int m_hp = 1;

    public float invFrames = 0.5f;
    private float invTimer = 0.0f;
    public bool invincible = false;


    public UnityEvent damageEvent;
    public UnityEvent deathEvent;
    public UnityEvent healEvent;

    public int HP
    {
        get
        {
            return m_hp;
        }
        set
        {
            m_hp = value;
            if(m_hp <= 0)
            {
                m_hp = 0;
                deathEvent.Invoke();
            }
            else if(m_hp > m_maxHP)
            {
                m_hp = m_maxHP;
            }
            for (int j = 0; j < m_maxHP; j++)
            {
                if(j+1 <= m_hp)
                {
                    m_containers[j].Full = true;
                }
                else
                {
                    m_containers[j].Full = false;
                }

            }
            
          
        }
    }

    public float m_imageMargin = 25.0f;

    public int m_heartsAcross = 5;


    // Use this for initialization
    void Start () {
        m_containers = new HeartContainer[m_maxContainers];
        m_hp = m_maxHP;
    
        for (int j = 0; j < m_maxContainers; j++)
        {
            GameObject h = Instantiate(m_heartPrefab, m_canvas.transform) as GameObject;
            m_containers[j] = h.GetComponent<HeartContainer>();
            RectTransform t = h.GetComponent<RectTransform>();

            Vector2 heartPos = new Vector2(t.rect.width * ((j % m_heartsAcross+1)) + (j % m_heartsAcross + 1) * m_imageMargin, -(t.rect.height * ((j / m_heartsAcross + 1)) + (j / m_heartsAcross + 1) * m_imageMargin));

            t.anchoredPosition = heartPos;

            if (j >= m_maxHP) h.SetActive(false);

        }
       
    }


    public void Damage(int amt)
    {
        if(!invincible)
        {
            damageEvent.Invoke();
            HP -= amt;
            invincible = true;
        }
        //TODO: sounds
       
    }
    public void Heal(int amt)
    {
        HP += amt;
        healEvent.Invoke();

    }

    // Update is called once per frame
    void Update () {
        if(invincible)
        {
            invTimer += Time.deltaTime;
            if(invTimer >= invFrames)
            {
                invincible = false;
                invTimer = 0.0f;
            }
        }

    }
}
