﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeartContainer : MonoBehaviour {

    public Sprite m_fullSprite;
    public Sprite m_emptySprite;
    private Image m_heartImage;

    private bool m_full = true;

    public bool Full
    {
        get
        {
            return m_full;
        }
        set
        {
            m_full = value;
            m_heartImage.sprite = (value) ? m_fullSprite : m_emptySprite;

        }
    }

	// Use this for initialization
	void Start () {
        m_heartImage = GetComponent<Image>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}



}
