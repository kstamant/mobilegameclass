﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CountedTrigger : MonoBehaviour {

    public int numToTrigger = 4;
    public int currentCount = 0;
    public bool loop = false;
    public bool triggerAfterEveryIncr = false;
    public UnityEvent onCountReached;
    
	
    public void Increment()
    {
        ++currentCount;
        if(currentCount == numToTrigger || triggerAfterEveryIncr && currentCount > numToTrigger)
        {
            if (loop) currentCount = 0;
            onCountReached.Invoke(); 
        }
    }
	
}
