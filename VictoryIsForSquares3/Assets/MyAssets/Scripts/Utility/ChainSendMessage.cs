﻿using UnityEngine;
using System.Collections;

public class ChainSendMessage : MonoBehaviour {

    ChainSendMessage[] m_next = null;
    public void Start()
    {
        m_next = GetComponentsInChildren<ChainSendMessage>();
    }

	public void ChainDownMessage(string funcName, object arg)
    {
        gameObject.SendMessage(funcName, arg, SendMessageOptions.DontRequireReceiver);
        if(m_next != null)
        {
            foreach(ChainSendMessage link in m_next)
            {
                link.ChainDownMessage(funcName, arg);
            }
        }
    }

}
