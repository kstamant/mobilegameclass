﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour {

    public void LoadScene(string scene)
    {
        SceneManager.LoadSceneAsync(scene);
    }
}
