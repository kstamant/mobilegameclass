﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class TextManager : MonoBehaviour {

    [SerializeField]
    private DialogueManager m_manager;

    //[SerializeField]
    //private Text m_textBox;

    private bool m_running = false;
    private float m_curTime = 0.0f;

    [SerializeField]
    private float m_tickTime = .3f;

    [SerializeField]
    private Text m_text;

    private DialogueMessage m_currentDialogue;
    private string m_message;
    private int m_length = 0;
    private int m_currentChar = 0;

    private bool m_speedBoost = false;

    //Use this for initialization
    void Start () {
	    
	}

    public void SetDialogue(DialogueMessage message)
    {
        m_currentDialogue = message;
        m_message = message.GetMessage();
        m_length = message.Length;
        m_text.text = "";
    }
	
    public void StartDialogue(DialogueMessage message = null)
    {

        if (message != null)
        {
            Debug.Log("Started dialogue: " + message.GetMessage());
            SetDialogue(message);
        }
        m_currentChar = 0;
        m_running = true;
    }

	// Update is called once per frame
	void Update () {
        if (m_running)
        {
            m_curTime += Time.deltaTime;
            if (m_curTime >= m_tickTime)
            {
                if (m_currentChar < m_length)
                {
                    //Debug.Log("printed: " + m_message.Substring(m_currentChar, 1));
                    m_text.text += m_message.Substring(m_currentChar, 1);
                    ++m_currentChar;
                    m_curTime = 0.0f;
                }
                else
                {
                    if (m_currentDialogue.Next != null)
                    {

                        StartDialogue(m_currentDialogue.Next);
                    }
                    else
                    {
                        m_running = false;
                        m_manager.DialogueFinished();
                    }
                    //TODO: send message that dialogue is done. bool?
                }
            }
        }

    }
}
