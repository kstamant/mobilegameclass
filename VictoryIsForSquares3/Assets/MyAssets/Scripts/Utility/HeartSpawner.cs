﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartSpawner : MonoBehaviour {
    public GameObject heart;
    public HealthManager player;
    public int numHitsToHeart = 10;
    private int hits = 0;
    public int Hits { get { return hits; } set { hits = value; if(hits >= numHitsToHeart) { hits -= numHitsToHeart;  SpawnHeart(); } } }

    void SpawnHeart()
    {
        GameObject t = Instantiate(heart, null) as GameObject;
        t.transform.position = transform.position;
        HeartHealing h = t.GetComponentInChildren<HeartHealing>();
        h.health = player;


    }
    // Use this for initialization
    void Start () {
		
	}
    public void Hit()
    {
        Hits++;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
