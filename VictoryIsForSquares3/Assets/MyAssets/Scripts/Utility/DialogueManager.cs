﻿using UnityEngine;
using System.Collections;

public class DialogueManager : MonoBehaviour {


    //private int m_currentDialogue = 0;

    // [SerializeField]
    // private DialogueMessage[] m_messages;

    

    [SerializeField]
    private TextManager m_textManager;

    [SerializeField]
    private GameObject m_textBox;
    public void StartDialogue(DialogueMessage message)
    {
        m_textBox.SetActive(true);
        m_textManager.StartDialogue(message);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void DialogueFinished()
    {
        m_textBox.SetActive(false);
    }


}
