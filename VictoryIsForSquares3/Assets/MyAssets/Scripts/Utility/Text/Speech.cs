﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityStandardAssets.CrossPlatformInput;
public class Speech : MonoBehaviour {

    [System.Serializable]
    public struct SpeechMessage
    {
        public GameObject message;
        public bool hasDuration;
        public bool overrideButton;

        public float duration;
        public UnityEvent onMessageShow;
        public UnityEvent onMessageFinish;
        

    }

    public bool autoAdvances = false;
    public string advanceButton = "Jump";



    public bool inputAdvancesText = false;
    public float defaultAdvanceTime = 0.1f;
    private bool readyForInput = true;

    public SpeechMessage[] messages;
   
    private int numMessages;
    private int currentMessage;
    private Coroutine currentCoroutine = null;
    public UnityEvent onAnyMessageShow;
    public UnityEvent onAnyMessageReadyForNext;
    public UnityEvent onAnyMessageCompleted;

    public UnityEvent onSpeechCompleted;
    public bool speaking = false;
    // Use this for initialization
    void Start () {
        for (int j = 0; j < messages.Length; ++j)
        {
            messages[j].message.SetActive(false);
            if (messages[j].duration <= 0.0f) messages[j].duration = defaultAdvanceTime;
        }
        currentMessage = 0;
        numMessages = messages.Length;
    }

    public IEnumerator DisplayMessage(float dur)
    {
        readyForInput = false;
        yield return new WaitForSeconds(dur);
        onAnyMessageReadyForNext.Invoke();
        readyForInput = true;
        currentCoroutine = null;
        yield break;
    }


    //public IEnumerator DisplayMessage(float dur)
    //{
    //    readyForInput = false;
    //    yield return new WaitForSeconds(dur);
    //    readyForInput = true;
    //    currentCoroutine = null;
    //    yield break;
    //}


    public void StartSpeech(bool start)
    {
        speaking = start;

        if (!start)
        {
            for (int j = 0; j < messages.Length; ++j)
            {
                messages[j].message.SetActive(false);
                //if (messages[j].duration <= 0.0f) messages[j].duration = defaultAdvanceTime;
            }
            currentMessage = 0;
        }
    }

    // Update is called once per frame
    void Update () {
		if(speaking)
        {
            if(readyForInput)
            {

                if(currentMessage == 0 || (inputAdvancesText && CrossPlatformInputManager.GetButtonUp(advanceButton)) || !inputAdvancesText || (currentMessage < numMessages && messages[currentMessage].overrideButton))
                {
                    

                    if(currentMessage > 0)
                    {
                        messages[currentMessage - 1].message.SetActive(false);
                        messages[currentMessage - 1].onMessageFinish.Invoke();
                        onAnyMessageCompleted.Invoke();

                    }
                   //else
                   //{
                   //    messages[currentMessage].message.SetActive(true);
                   //    messages[currentMessage].onMessageShow.Invoke();
                   //    onAnyMessageCompleted.Invoke();
                   //}



                    if (currentMessage == numMessages)
                    {
                        onSpeechCompleted.Invoke();
                        readyForInput = false;
                        speaking = false;
                    }
                    else
                    {
                        messages[currentMessage].message.SetActive(true);
                        currentCoroutine = StartCoroutine(DisplayMessage(messages[currentMessage].duration));
                        messages[currentMessage].onMessageShow.Invoke();
                        onAnyMessageShow.Invoke();
                        ++currentMessage;
                    }
                    
                }
               
            }
            
        }
    }


}


//[CustomEditor(typeof(Speech))]
//[CanEditMultipleObjects]
//public class MyScriptEditor : Editor
//{
//    SerializedProperty time;
//    void OnEnable()
//    {
//        // Setup the SerializedProperties.
//        time = serializedObject.FindProperty("damage");
//    }
//
//
//
//    public override void OnInspectorGUI()
//    {
//
//
//
//        serializedObject.Update();
//
//
//        EditorGUILayout.FloatField
//
//        SerializedProperty serProp = serializedObject.FindProperty("lookAtPoint")
//
//        var myScript = target as MyScript;
//
//        myScript.flag = GUILayout.Toggle(myScript.flag, "Flag");
//
//        if (myScript.flag)
//            myScript.i = EditorGUILayout.IntSlider("I field:", myScript.i, 1, 100);
//
//    }
//}
