﻿using UnityEngine;
using System.Collections;

public class DialogueMessage : MonoBehaviour {


    public int Length { get; private set; }

    [SerializeField]
    private string m_message = "";

    [SerializeField]
    public DialogueMessage Next;

    [SerializeField]
    private DialogueManager m_manager;
    [SerializeField]
    private string m_eventMethod = "";


    public void ShowDialogue()
    {
        m_manager.StartDialogue(this);
    }
   // Use this for initialization
   void Start () {
        Length = m_message.Length;
	}

    public void NotifyFinished()
    {
        if(!m_eventMethod.Equals(""))
        {
            gameObject.SendMessage(m_eventMethod);
        }
    }


    public string GetMessage()
    {
        return m_message;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

}
