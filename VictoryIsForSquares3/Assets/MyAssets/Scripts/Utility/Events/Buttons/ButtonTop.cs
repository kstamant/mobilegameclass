﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonTop : MonoBehaviour
{
    private Animator animator;
    public string animationToPlay = "";
    public string tagToDetect = "";

    void Start()
    {
        animator = GetComponent<Animator>();

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag(tagToDetect) && !animationToPlay.Equals(""))
        {
            animator.Play(animationToPlay);
        }
    }

}
