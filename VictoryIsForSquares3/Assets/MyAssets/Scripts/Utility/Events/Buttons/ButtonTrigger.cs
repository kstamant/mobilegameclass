﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ButtonTrigger : MonoBehaviour
{

    public UnityEvent triggeredFunctions;
    public string tagToDetect;// = "Player";
    public bool m_oneShot = false;
    public bool m_prevTriggered = false;

    void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log("Triggered");
        if (other.CompareTag(tagToDetect))
        {
            if (!m_oneShot || (m_oneShot && !m_prevTriggered))
            {
                triggeredFunctions.Invoke();
                m_prevTriggered = true;
            }
        }
    }


}

