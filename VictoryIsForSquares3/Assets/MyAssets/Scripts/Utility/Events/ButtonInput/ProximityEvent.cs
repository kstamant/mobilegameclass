﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityStandardAssets.CrossPlatformInput;


public class ProximityEvent : MonoBehaviour {

    public string playerTag = "Player";

    public string inputButton = "Talk";

    public float inputWait = 0.75f;
    public float delay = 0.0f;
    public bool infiniteUses = false;
    public int numShots = 1;
    public bool triggerOnEnter = false;

    public UnityEvent immediateTriggeredEvent;
    public UnityEvent triggeredEvent;
    public UnityEvent proximityEnterEvent;
    public UnityEvent proximityExitEvent;

    private bool playerInRange = false;

    private bool triggered = false;
    private bool eventFired = false;
    private float delayTimer = 0.0f;
    private float inputWaitTimer = 0.0f;
    private int eventFiredCount = 0;


    void OnTriggerEnter2D(Collider2D other)
    {
        if((infiniteUses || numShots > 0) && other.CompareTag(playerTag))
        {
            playerInRange = true;
            proximityEnterEvent.Invoke();
            if(triggerOnEnter)
            {
                eventFiredCount++;
                triggered = true;
                --numShots;
            }
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag(playerTag))
        {
            if(!triggered)
            {

            
            playerInRange = false;
            delayTimer = 0.0f;
            inputWaitTimer = 0.0f;
            triggered = false;
            eventFired = false;
            proximityExitEvent.Invoke();
            }
        }
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (playerInRange && numShots > 0) {

            if (inputWaitTimer < inputWait)
            {
                inputWaitTimer += Time.deltaTime;
            }
            else
            {
                
                if (!triggered && ((inputButton.Equals(""))? false : CrossPlatformInputManager.GetButtonUp(inputButton)))
                {
                    immediateTriggeredEvent.Invoke();
                    triggered = true;
                }

                if (!eventFired && triggered)
                {
                    delayTimer += Time.deltaTime;
                    if (delayTimer >= delay)
                    {
                        triggeredEvent.Invoke();
                        if (!infiniteUses) --numShots;
                        eventFiredCount++;
                        inputWaitTimer = 0.0f;
                        delayTimer = 0.0f;
                        playerInRange = false;
                    }

                }
            }

        }

	}
}
