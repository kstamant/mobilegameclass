﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityStandardAssets.CrossPlatformInput;

public class JoystickInputDetector : MonoBehaviour {

    public string button;
    public UnityEvent onButtonPushed;
	// Update is called once per frame
	void Update () {
		if(CrossPlatformInputManager.GetButtonUp(button))
        {
            onButtonPushed.Invoke();
        }
	}
}
