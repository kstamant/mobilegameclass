﻿using UnityEngine;
using System.Collections;

//public delegate void 

public struct SquaresCustomEvent
{
    string m_eventName;


}

public class TriggerEvent : MonoBehaviour {

    [SerializeField]
    private string m_tagToDetect;

    [SerializeField]
    private string m_eventNames;

    [SerializeField]
    private GameObject m_obj;
    // Use this for initialization


    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag(m_tagToDetect))
        {
            m_obj.SendMessage(m_eventNames, SendMessageOptions.DontRequireReceiver);
        }
    }

    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
