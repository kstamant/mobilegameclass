﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomOutSmooth : MonoBehaviour {

    public float zoomDuration = 1.0f;
    public float zoomOutDuration = 2.0f;
    public float size = 16.0f;
    public float originalSize = 8.0f;
    public bool triggered = false;
    public bool inverseTriggered = false;
    private float timer = 0.0f;

    public Camera affected_camera;

    public void Reset()
    {
        timer = 0.0f;
        triggered = false;
    }
    // Use this for initialization
    void Start () {
        //affected_camera = Camera.main;
        //originalSize = affected_camera.orthographicSize;
    }
	
    public void Trigger()
    {
        triggered = true;
    }

    public void Revert()
    {
        inverseTriggered = true;
    }

    // Update is called once per frame
    void Update () {
		if(triggered)
        {
            timer += Time.deltaTime;
            
            if (timer >= zoomDuration)
            {

                affected_camera.orthographicSize = size;
                Reset();
            }
            else
            {
                affected_camera.orthographicSize = Mathf.Lerp(originalSize, size, (timer / zoomDuration));
            }
        }
        else if(inverseTriggered)
        {
            timer += Time.deltaTime;

            if (timer >= zoomOutDuration)
            {

                affected_camera.orthographicSize = originalSize;
                timer = 0.0f;
                inverseTriggered = false;
            }
            else
            {
                affected_camera.orthographicSize = Mathf.Lerp(size, originalSize, (timer / zoomOutDuration));
            }
        }

	}
}
