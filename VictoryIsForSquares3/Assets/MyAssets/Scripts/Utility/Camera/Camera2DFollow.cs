﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Transform))]
public class Camera2DFollow : MonoBehaviour {

    private Transform m_cameraTransform;

    public bool Enabled { get; set; }


    public Transform m_target;

    [SerializeField]
    private float m_followDamping = 1.0f;
    [SerializeField]
    private float m_followMin = 0.02f;

    [SerializeField]
    private float m_followSpeed = 1.0f;

    private bool m_hasDamping;


    public void SetTarget(Transform t)
    {
        m_target = t;
    }

    public void SetSpeed(float f)
    {
        m_followSpeed = f;
    }


    // Use this for initialization
    void Start () {
        Enabled = false;
        m_cameraTransform = transform;
        UpdateHasDamping();
    }
    private void UpdateHasDamping()
    {
        m_hasDamping = ((m_followDamping == 1.0) ? false : true);
    }

    public void SetDamping(float damping)
    {
        m_followDamping = damping;
        UpdateHasDamping();
    }
	
	// Update is called once per frame
	void Update () {
	    if(m_target != null)
        {
            if (Vector2.Distance(m_cameraTransform.position, m_target.position) >= m_followMin)
            {
                
                Vector3 camPos = new Vector3(m_cameraTransform.position.x, m_cameraTransform.position.y, m_target.position.z);
                Vector2 dir = (m_target.position - camPos).normalized;
                if (m_hasDamping)
                {
                    Vector2 newPos = Vector2.Lerp(camPos, m_target.position, (1.0f - m_followDamping) * m_followSpeed * Time.deltaTime);

                    Vector2 newDir = (m_target.position - new Vector3(newPos.x, newPos.y,m_target.position.z)).normalized;

                    if(Vector2.Dot(dir,newDir) < 0.0f)
                    {
                        newPos = new Vector2(m_target.position.x, m_target.position.y);
                    }

                    m_cameraTransform.position = new Vector3(newPos.x, newPos.y, -10.0f);
                }
                else
                {
                    m_cameraTransform.position = new Vector3(m_target.position.x, m_target.position.y, -10.0f);

                }
            }
        }
	}


}
