﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class Jitter : MonoBehaviour {

    private enum JitterDir { POS, NEG, RET_P, RET_N, NEUTR}
    public Vector2 jitterDirection;
    public float amount = 0.01f;
    public float halfTime = .01f;
    public int iters = 1;
    public bool jittering = false;
    private JitterDir dir = JitterDir.NEUTR;
    public UnityEvent onJitterFinished;
    Coroutine jitCo;

    // Use this for initialization
    void Start () {
        jitterDirection = jitterDirection.normalized;

    }
    public void StartJitter()
    {
        if(jitCo == null) jitCo = StartCoroutine(jitterCoroutine());
        // curJitterDir;

    }

    public IEnumerator jitterCoroutine()
    {

        for (int j = 0; j < iters; j++)
        {
            dir = JitterDir.POS;
            yield return new WaitForSeconds(halfTime);

            dir = JitterDir.RET_P;
            yield return new WaitForSeconds(halfTime);

            dir = JitterDir.NEG;
            yield return new WaitForSeconds(halfTime);

            dir = JitterDir.RET_N;
            yield return new WaitForSeconds(halfTime);
        }

        //yield return new WaitForSeconds(halfTime/2.0f);

        dir = JitterDir.NEUTR;
        jitCo = null;
        onJitterFinished.Invoke();
        yield break;
    }



	// Update is called once per frame
	void Update () {
        switch (dir)
        {
            case JitterDir.NEUTR:
                break;
            case JitterDir.POS:
                Vector2 delt = (jitterDirection  * amount / halfTime) * Time.deltaTime;
                transform.position += new Vector3(delt.x, delt.y,0);
                break;

            case JitterDir.RET_P:
                Vector2 delt2 = -(jitterDirection * amount / halfTime) * Time.deltaTime;
                transform.position += new Vector3(delt2.x, delt2.y, 0);
                break;

            case JitterDir.NEG:
                Vector2 delt3 = -(jitterDirection * amount / halfTime) * Time.deltaTime;
                transform.position += new Vector3(delt3.x, delt3.y, 0);
                break;
          
            case JitterDir.RET_N:
                Vector2 delt4 = (jitterDirection * amount / halfTime) * Time.deltaTime;
                transform.position += new Vector3(delt4.x, delt4.y, 0);
                break;
            default:
                break;
        }
    }


}
