﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turntable : MonoBehaviour {

    public float startRotation = -90.0f;
    public float endRotation = 90.0f;


    public float turnDuration = 2.0f;
    public float pauseDuration = 1.0f;
    public bool active = true;

    private float turnTimer = 0.0f;
    private float pauseTimer = 0.0f;
    private bool isTurning = false;

    private float angle;
    private float turnAngle;

    private bool turningRight = true;



    // Update is called once per frame
    void Update () {
		
        if(active)
        {
            if(isTurning)
            {
                turnTimer += Time.deltaTime;
                
                angle += (turningRight) ?  ((endRotation - startRotation)/ turnDuration) * Time.deltaTime : ((endRotation - startRotation) / turnDuration) * -Time.deltaTime;
                if (turnTimer >= turnDuration)
                {
                    turnTimer = 0.0f;
                    angle = (turningRight) ? endRotation : startRotation;
                    turningRight = !turningRight;
                    isTurning = false;
                }


               

                transform.rotation = Quaternion.Euler(0, 0, angle);
            }
            else
            {
                pauseTimer += Time.deltaTime;

                if(pauseTimer >= pauseDuration)
                {
                    isTurning = true;
                    pauseTimer = 0.0f;
                }


            }
        }



	}



}
