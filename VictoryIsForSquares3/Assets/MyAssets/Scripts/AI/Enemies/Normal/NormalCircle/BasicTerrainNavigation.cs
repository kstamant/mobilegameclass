﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicTerrainNavigation : MonoBehaviour {
    public NormalEnemy m_ai;
    public BasicCircleMovement m_movement;
    public LayerMask m_whatIsGround;

    public Transform m_groundCheck;
    public Vector2 m_groundCheckSize;

    public bool m_checkOnStay = true;
    // Use this for initialization
    void Start () {
		
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (!m_checkOnStay)
        {
            if (((1 << other.gameObject.layer) & m_whatIsGround.value) > 0)
            {
                if (Physics2D.OverlapBox(m_groundCheck.position, m_groundCheckSize, 0, m_whatIsGround) != null)
                {
                    m_movement.Jump();
                }
            }
        }

    }


    void OnTriggerStay2D(Collider2D other)
    {
        if (m_checkOnStay)
        {
            if (((1 << other.gameObject.layer) & m_whatIsGround.value) > 0)
            {
                if (Physics2D.OverlapBox(m_groundCheck.position, m_groundCheckSize, 0, m_whatIsGround) != null)
                {
                    m_movement.Jump();
                }
            }
        }
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
