﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicCircleMovement : MonoBehaviour {
    public NormalEnemy m_ai;

    public float m_moveSpeed = 2.0f;
    public float m_jumpForce = 50.0f;

    public float m_maxSpeed = 100.0f;
    public Rigidbody2D m_rigidbody;
    public bool m_moving = false;
    public bool m_canMove = true;
    public BasicAggro m_aggro;


    public void Jump()
    {
        m_rigidbody.AddForce(new Vector2(0.0f, m_jumpForce));
    }

    // Update is called once per frame
    void Update() {
        if (m_ai.m_isAlive)
        {
            if (m_canMove && m_ai.m_isAggroed)
            {
                m_rigidbody.AddForce(new Vector2(m_aggro.m_playerDirection.x, 0.0f).normalized * m_moveSpeed);
                if(Mathf.Abs(m_rigidbody.velocity.x) > m_maxSpeed)
                {
                    m_rigidbody.velocity = new Vector2((m_rigidbody.velocity.x > 0.0f) ? m_maxSpeed : -m_maxSpeed, m_rigidbody.velocity.y);
                }

            }
        }
	}
}
