﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicAggro : MonoBehaviour {

    public Transform m_player;
    public NormalEnemy m_ai;
    public string m_tagToDetect = "Player";

    public Vector3 m_playerDirection;

    // Use this for initialization
    void Start () {
        m_playerDirection = Vector3.zero;

    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if(!m_ai.m_isAggroed && other.CompareTag(m_tagToDetect))
        {
            m_player = other.gameObject.transform;
            m_ai.m_isAggroed = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (m_ai.m_isAggroed && other.CompareTag(m_tagToDetect))
        {
            m_ai.m_isAggroed = false;
        }
    }


    // Update is called once per frame
    void Update () {
		if(m_ai.m_isAggroed)
        {
            m_playerDirection = (m_player.position - transform.position).normalized;
        }

    }
}
