﻿using UnityEngine;
using System.Collections;


public class NormalEnemy : MonoBehaviour {

    // private HealthSystem m_health;
    public bool m_isAlive = true;


    public BasicAggro m_aggro;
    public bool m_isAggroed = false;

    public BasicTerrainNavigation m_terrainNavigator;

    public BasicCircleMovement m_movement;

    public Rigidbody2D m_rigidbody;

    //public bool IsAggroed
    //{
    //    get
    //    {
    //        return m_isAggroed;
    //    }
    //    set
    //    {
    //        //TODO: add extra meth
    //        m_isAggroed = value;
    //    }
    //}


    // Use this for initialization
    void Start() {
        m_terrainNavigator.m_ai = m_aggro.m_ai = m_movement.m_ai = this;
        m_movement.m_aggro = m_aggro;
        m_movement.m_rigidbody = m_rigidbody;
        m_terrainNavigator.m_movement = m_movement;
    }
	
	// Update is called once per frame
	void Update() {
	
	}

    
}
