﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyHealth : MonoBehaviour {
    public UnityEvent damageAnimEvent;
    public UnityEvent deathAnimEvent;
    public UnityEvent deathEvent;
    public float m_maxHP = 1.0f;
    public float m_hp = 1.0f;
    public float deathDelay = 1.0f;
    

    public float HP
    {
        get
        {
            return m_hp;
        }
        set
        {
            if (m_hp > 0.0f && value <= 0.0f)
            {
                deathAnimEvent.Invoke();
                Invoke("Die", deathDelay);
            }

            m_hp = value;

        }
    }

    void Die()
    {
        deathEvent.Invoke();
    }
    // Use this for initialization
    void Start () {
        m_hp = m_maxHP;

    }
	
    public void Damage(float amt)
    {
        damageAnimEvent.Invoke();
        HP -= amt;
    }

	// Update is called once per frame
	void Update () {
		
	}
}
