﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDetection : MonoBehaviour {

    public LayerMask canBeHurtBy;
    public float invincibilityTime = 1.0f;
    public EnemyHealth m_health;
    // ( (1 << other.gameObject.layer) & canBeHurtBy.value) > 0

    void OnTriggerEnter2D(Collider2D other)
    {
        if(((1 << other.gameObject.layer) & canBeHurtBy.value) > 0)
        {
            float damage = 1.0f;
            PlayerAttackInfo info = other.GetComponent<PlayerAttackInfo>();
            if (info != null) damage = info.damage;
            m_health.Damage(damage);
            Debug.Log("An enemy was hit for " + damage + " damage.");
        }
    }
   // void OnTriggerStay2D(Collider2D other)
   // {
   //     if (((1 << other.gameObject.layer) & canBeHurtBy.value) > 0)
   //     { 
   //         m_health.Damage(0);
   //
   //     }
   //
   // }


    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
