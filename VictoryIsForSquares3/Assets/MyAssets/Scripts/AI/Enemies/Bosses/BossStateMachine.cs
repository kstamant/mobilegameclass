﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.MyAssets.SharedVariables;
using UnityEngine.Events;




public class BossStateMachine : MonoBehaviour {


    [System.Serializable]
    public struct BossAttack
    {
        public bool isEnabled;
        public AttackState state;
        public float startTime;
        public bool triggered;
        public UnityEvent triggers;

    }
    UnityEvent m_triggers;

    public BossAttack[] m_attacks;

    public EnemyHealth m_health;
    public WormState m_state;

    public float loopTime = 10.0f;
    public float timer = 0.0f;
    public float attackTimer = 0.0f;
    public bool m_enabled;

    private int m_numSets;

    public float attackWaitTimer = 0.0f;
    public float attackWait = 0.0f;


    public float damageSpeedModifier = 0.0f;


    // Use this for initialization
    void Start()
    {
      
    }
    public void EnableBoss()
    {
        m_state = WormState.WAITING;
    }

    public void DisableBoss()
    {
        m_state = WormState.HIDING;
    }
    public void DoTriggers()
    {
        m_triggers.Invoke();
        m_triggers = null;
    }

    void DoPassiveUpdate()
    {
        attackTimer += Time.deltaTime + (damageSpeedModifier * (1.0f - m_health.m_hp/m_health.m_maxHP) * Time.deltaTime);
        if (attackTimer >= loopTime)
        {
            attackTimer = 0.0f;
            for (int j = 0; j < m_attacks.Length; ++j)
            {
                ResetSet(ref m_attacks[j]);
            }
        }

        for (int j = 0; j < m_attacks.Length; ++j)
        {
  
            if (m_attacks[j].isEnabled && !m_attacks[j].triggered && m_attacks[j].startTime <= attackTimer)
            {
               
                m_attacks[j].triggered = true;
                m_attacks[j].triggers.Invoke();

            }
                
            }
        }

    void ResetSet(ref BossAttack attackSet)
    {
        attackSet.triggered = false;
      
    }


    void DoAttackUpdate()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if (m_enabled) timer += Time.deltaTime;

        switch (m_state)
        {
            case WormState.DEAD:
                break;
            case WormState.WAITING:
                DoPassiveUpdate();
                break;
            case WormState.ATTACKING:
                DoAttackUpdate();
                break;
            case WormState.DYING:

                break;
            case WormState.DAMAGED:

                break;
            case WormState.FIRING:

                break;
            default:
                break;
        }

    }


}
