﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossHealthBarManager : MonoBehaviour {

    public enum HPstate { HEALTHY, WOUNDED, PERIL };
    public HPstate state = HPstate.HEALTHY;

    public EnemyHealth m_health;
    public Image hp_renderer;


    public Sprite healthy;
    public Sprite wounded;
    public Sprite peril;

    public float width = 60.0f;
    public float height = 10.0f;

    public void UpdateHP()
    {
        //bar.rect.width = // new Vector3(30.0f - (maxHealth - health) / (maxHealth / 30.0f), 2.6625f, 2.6625f);

        hp_renderer.rectTransform.sizeDelta = new Vector2(width * (m_health.m_hp / m_health.m_maxHP) - (width/m_health.m_maxHP), height);
        //bar.rect.width = width;
        if (m_health.m_hp / m_health.m_maxHP <= 0.333f)
        {
            state = HPstate.PERIL;
            hp_renderer.sprite = peril;

        }
        else if (m_health.m_hp / m_health.m_maxHP <= 0.666f)
        {
            state = HPstate.WOUNDED;
            hp_renderer.sprite = wounded;

        }
       
    }

    public void DisplayBar(bool on)
    {
        hp_renderer.gameObject.SetActive(on);
    }


    void Start()
    {
        hp_renderer.sprite = healthy;
        hp_renderer.rectTransform.sizeDelta = new Vector2(width, height);
        //hp_renderer.gameObject.SetActive(false);
        
        state = HPstate.HEALTHY;
    }
    // Update is called once per frame
    void Update () {
		
	}
}
