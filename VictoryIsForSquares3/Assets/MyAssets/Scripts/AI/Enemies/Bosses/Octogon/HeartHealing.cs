﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartHealing : MonoBehaviour {

    public HealthManager health;

    void OnTriggerEnter2D(Collider2D other)
    {
        health.Heal(1);
        Destroy(gameObject);

    }
}
