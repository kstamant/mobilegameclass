﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitTowards : MonoBehaviour {


    public Transform target;
    public Rigidbody2D rigid;
    Vector3 posP;

    public void HitTowardsWithForce(float force)
    {
        rigid.AddForce((posP - rigid.transform.position).normalized * force);
    }
    public void UpdatePlayerPos()
    {
        posP = target.position;

    }

    // Update is called once per frame
    void Start () {
        posP = target.position;

    }
}
