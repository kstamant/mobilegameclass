﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityStandardAssets.CrossPlatformInput;
using Assets.MyAssets.SharedVariables;

public class BossSpinTrigger : MonoBehaviour {

    public UnityEvent events;
    public AudioSource attackSound;

    //x offset: -0.06
    public void SpinAttack()
    {

       
         attackSound.Play();

         events.Invoke();


    }

}
