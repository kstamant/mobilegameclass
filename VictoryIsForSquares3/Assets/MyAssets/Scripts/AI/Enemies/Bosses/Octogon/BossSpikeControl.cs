﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpikeControl : MonoBehaviour {

    public string attackAnim;
    public Animator[] animators;
    public Animator animator;
    public Animator playerAnimator;
    public Animation spinAnim;
    public ScaleAnimation[] scales;


    // Use this for initialization
    void Start()
    {
        animators = GetComponentsInChildren<Animator>();
        scales = GetComponentsInChildren<ScaleAnimation>();
        animator = GetComponent<Animator>();
        spinAnim = GetComponent<Animation>();
       
    }



    public void SetTriggers(string trigger)
    {

        animator.Play(attackAnim);

        foreach (ScaleAnimation s in scales)
        {
            s.SetScale(1.0f);
        }
        foreach (Animator a in animators)
        {
            if (!a.Equals(animator) && !a.Equals(playerAnimator)) a.SetTrigger(trigger);
        }
    }
}
