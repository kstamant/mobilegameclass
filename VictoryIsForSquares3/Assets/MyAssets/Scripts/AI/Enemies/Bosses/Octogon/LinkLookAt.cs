﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkLookAt : MonoBehaviour {


    public Transform player;

	// Use this for initialization
	void Start () {
        LookAtPlayer[] lookAts = GetComponentsInChildren<LookAtPlayer>();
        foreach (LookAtPlayer l in lookAts)
        {
            l.target = player;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
