﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour {

    public Vector3 offsetVec;

    public Transform target;
    public float offset;
    public bool isLooking = true;
    public bool restrict = false;
    // Update is called once per frame
    void Update () {
        if (isLooking)
        {
            Vector3 totalOffset = (target.position - (transform.parent.position + offsetVec));
            totalOffset = offsetVec + new Vector3(totalOffset.x, ((restrict)?((totalOffset.y < 0.0f) ?  totalOffset.y : -totalOffset.y) : totalOffset.y), totalOffset.z).normalized * offset * transform.lossyScale.x;
            transform.position = transform.parent.position + totalOffset;
        }
	}
}
