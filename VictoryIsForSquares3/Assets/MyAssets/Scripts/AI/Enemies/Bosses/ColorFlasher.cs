﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorFlasher : MonoBehaviour {

    public SpriteRenderer sprite;

    public Color flashCol;
    private Color originalCol;

    public float time = .1f;
    public int iters = 4;
    Coroutine co;
    void Start()
    {
        originalCol = sprite.color;
    }

    public void Flash()
    {
        if (sprite.gameObject.activeInHierarchy && gameObject.activeInHierarchy)
        {
            if (co == null) co = StartCoroutine(FlashCoroutine());
            else
            {
                StopCoroutine(co);
                co = StartCoroutine(FlashCoroutine());
            }
        }
    }

    public IEnumerator FlashCoroutine()
    {

        for (int i = 0; i < iters; ++i)
        {
            //sprite.color = Color.Lerp(flashCol,originalCol, (((float)i / (float)iters)));
            //yield return new WaitForSeconds(time/(float)iters);

            if ((i + 1) % 2 == 0) {
                sprite.color = flashCol;

            }
            else
            {
                sprite.color = originalCol;

            }
            yield return new WaitForSeconds(time / (float)iters);
        }

        sprite.color = originalCol;
        yield break;
    }
	

}
