﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormMissile : MonoBehaviour {


    public float speed = 50.0f;

    public bool firing = false;
    public Vector2 dir;
    //float lifetime = 2.0f;
    // Use this for initialization
   // Rigidbody2D rigidbod;
     
    void OnEnable(){
        //rigidbod = GetComponent<Rigidbody2D>();
    }

    public void Fire(Vector3 dir)
    {
        firing = true;
        this.dir = dir;
        transform.up = dir;
        //rigidbod.velocity = Vector2.zero;

        //rigidbod.AddForce(dir * speed);
        //.Translate(dir * speed * Time.deltaTime, Space.World);

    }
	
	// Update is called once per frame
	void Update () {
		if(firing)
        {
            transform.Translate(dir * speed * Time.deltaTime,Space.World);
            
        }
	}
}
