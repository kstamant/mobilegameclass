﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.MyAssets.SharedVariables;
using UnityEngine.Events;


public class CircleWormControl : MonoBehaviour {



    [System.Serializable]
    public struct WormAttack
    {
        public bool isEnabled;
        public AttackState state;
        public float startTime;
        public bool triggered;
        public bool repeat;
        public float duration;
        public UnityEvent triggers;
        public float triggerDelay;

    }
    [System.Serializable]
    public struct WormAttackSet
    {
        public bool isEnabled;
        public WormAttack[] attacks;
        public int numAttacks;
        public float startTime;
        public bool started;
        public bool repeat;
        public float timer;
        public float interval;
    }
    UnityEvent m_triggers;

    public WormAttacks m_attacks;

    //private GameObject[] m_segments;




    //public void 
    public WormAttackSet[] m_attackSets;


    public WormState m_state;

    public float loopTime = 10.0f;
    public float timer = 0.0f;
    public float attackTimer = 0.0f;
    public bool m_enabled;

    private int m_numSets;

    public float attackWaitTimer = 0.0f;
    public float attackWait = 0.0f;


    // Use this for initialization
    void Start () {
        //m_segments = m_attacks.segments;
        for(int j = 0; j < m_attackSets.Length; ++j)
        {
            m_attackSets[j].numAttacks = m_attackSets[j].attacks.Length;
        }
    }
    public void EnableWorm()
    {
        m_state = WormState.WAITING;
    }

    public void DisableWorm()
    {
        m_state = WormState.HIDING;
    }
    public void DoTriggers()
    {
        m_triggers.Invoke();
        m_triggers = null;
    }

    void DoPassiveUpdate()
    {
        attackTimer += Time.deltaTime;
        if (attackTimer >= loopTime)
        {
            attackTimer -= loopTime;
            for (int j = 0; j < m_attackSets.Length; ++j)
            {
                ResetSet(ref m_attackSets[j]);
            }
        }

        for (int j = 0; j < m_attackSets.Length; ++j)
        {
            if (!m_attackSets[j].started && m_attackSets[j].startTime < attackTimer)
            {
                m_attackSets[j].started = true;
            }
            
            if(m_attackSets[j].started)
            {
                m_attackSets[j].timer += Time.deltaTime;

                for (int k = 0; k < m_attackSets[j].numAttacks; ++k)
                {
                    if(m_attackSets[j].attacks[k].isEnabled && !m_attackSets[j].attacks[k].triggered && m_attackSets[j].attacks[k].startTime <= m_attackSets[j].timer)
                    {
                        m_state = WormState.ATTACKING;
                        m_attackSets[j].attacks[k].triggered = true;
                        m_triggers = m_attackSets[j].attacks[k].triggers;
                        Invoke("DoTriggers", m_attackSets[j].attacks[k].triggerDelay);
                        attackWait = m_attackSets[j].attacks[k].duration;
                        m_attacks.SetState(m_attackSets[j].attacks[k].state);
                    }
                }
            }
        }
    }

    void ResetSet(ref WormAttackSet attackSet)
    {
        attackSet.timer = 0.0f;
        for (int k = 0; k < attackSet.numAttacks; ++k)
        {
            attackSet.timer = 0.0f;
            attackSet.attacks[k].triggered = false;
        }
    }


    void DoAttackUpdate()
    {
        attackWaitTimer += Time.deltaTime;
        if(attackWaitTimer >= attackWait)
        {
            attackWaitTimer = 0.0f;
            attackWait = 0.0f;
            m_state = WormState.WAITING;
        }

    }

	// Update is called once per frame
	void Update () {

        //if (m_enabled) timer += Time.deltaTime;

        switch (m_state)
        {
            case WormState.DEAD:
                break;
            case WormState.WAITING:
                DoPassiveUpdate();
                break;
            case WormState.ATTACKING:
                DoAttackUpdate();
                break;
            case WormState.DYING:

                break;
            case WormState.DAMAGED:

                break;
            case WormState.FIRING:

                break;
            default:
                break;
        }

    }
}
