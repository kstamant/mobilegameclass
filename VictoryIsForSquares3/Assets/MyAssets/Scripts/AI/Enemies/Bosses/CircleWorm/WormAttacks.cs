﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.MyAssets.SharedVariables;
public class WormAttacks : MonoBehaviour {

    public WormMissileManager miss_manager;

    public AttackState m_state = AttackState.WAITING;
    public GameObject[] segments;
    public GameObject[] segmentAttackLocations;
    public GameObject[] segmentSlamLocations;

    public Vector3[] startingPositionsLocal;

    //public bool attacking = false;
    //public bool returning = false;
    //public bool returningFromSlamming = false;
    //public bool slamming = false;
    public float attackDuration = 10.0f;
    public float returnDuration = 10.0f;

    public float slamDuration = 1.0f;
    public float slamReturnDuration = 1.0f;
    private float timer = 0.0f;

    // Use this for initialization
    void Start ()
    {
        startingPositionsLocal = new Vector3[segments.Length];

        for (int j = 0; j < segments.Length; j++)
        {
            startingPositionsLocal[j] = segments[j].transform.localPosition;
        }

    }

    public void SetState(AttackState state)
    {
        timer = 0.0f;
        m_state = state;
    }

    public void Attack()
    {
        if (m_state == AttackState.WAITING)
        {
            timer = 0.0f;
            m_state = AttackState.ATTACKING;
        }
    }

    public void Slam()
    {
        if (m_state == AttackState.WAITING)
        {
            timer = 0.0f;
            m_state = AttackState.SLAMMING;
        }
    }
    void DoSlam()
    {
        timer += Time.deltaTime;
        timer = Mathf.Min(timer, slamDuration);
        for (int j = 0; j < segments.Length; j++)
        {
            segments[j].transform.localPosition = Vector2.Lerp(startingPositionsLocal[j], segmentSlamLocations[j].transform.localPosition, timer / slamDuration);
        }

        if (timer >= slamDuration)
        {
            timer = 0.0f;
            m_state = AttackState.RETURN_SLAMMING;
        }
    }

    void DoSlamReturn()
    {

        timer += Time.deltaTime;
        timer = Mathf.Min(timer, slamReturnDuration);
        for (int j = 0; j < segments.Length; j++)
        {
            segments[j].transform.localPosition = Vector2.Lerp(segmentSlamLocations[j].transform.localPosition, startingPositionsLocal[j], timer / slamReturnDuration);
        }

        if (timer >= slamReturnDuration)
        {
            timer = 0.0f;
            m_state = AttackState.WAITING;
        }

    }

    void DoAttack()
    {
        timer += Time.deltaTime;
        timer = Mathf.Min(timer, attackDuration);
        for (int j = 0; j < segments.Length; j++)
        {
            segments[j].transform.localPosition = Vector2.Lerp(startingPositionsLocal[j], segmentAttackLocations[j].transform.localPosition, timer / attackDuration);
        }

        if (timer >= attackDuration)
        {
            timer = 0.0f;
            m_state = AttackState.RETURNING;


            miss_manager.FireMissiles();

        }
    }

    void DoReturn()
    {

        timer += Time.deltaTime;
        timer = Mathf.Min(timer, returnDuration);
        for (int j = 0; j<segments.Length; j++)
        {
            segments[j].transform.localPosition = Vector2.Lerp(segmentAttackLocations[j].transform.localPosition, startingPositionsLocal[j], timer / returnDuration);
        }
        
        if (timer >= returnDuration)
        {
            timer = 0.0f;
            m_state = AttackState.WAITING;
        }

    }
    void Update ()
    {

        switch (m_state)
        {
            case AttackState.WAITING:
                break;
            case AttackState.ATTACKING:
                DoAttack();
                break;
            case AttackState.RETURNING:
                DoReturn();
                break;
            case AttackState.SLAMMING:
                DoSlam();
                break;
            case AttackState.RETURN_SLAMMING:
                DoSlamReturn();
                break;
            case AttackState.FIRING:
                break;
            default:
                break;
        }

	}


}
