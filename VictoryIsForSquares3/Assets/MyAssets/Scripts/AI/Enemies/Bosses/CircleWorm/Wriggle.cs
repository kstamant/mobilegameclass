﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wriggle : MonoBehaviour
{

    public float swaySpeed = 1.0f;
    public Vector2 swayDir = new Vector2(0, 1);

    //private float sway; //= 1.0f;
    public float flipTime = 1.0f;
    private float currentTime = 0.0f;

    // Use this for initialization
    void Start()
    {
        swayDir = swayDir.normalized;
        //sway = swaySpeed;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(swayDir * swaySpeed * Time.deltaTime,Space.World);
        currentTime += Time.deltaTime;

        if (currentTime >= flipTime)
        {
            currentTime = 0.0f;
            swaySpeed = -swaySpeed;
        }
    }

}