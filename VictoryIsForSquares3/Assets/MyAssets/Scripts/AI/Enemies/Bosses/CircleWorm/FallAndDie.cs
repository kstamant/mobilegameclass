﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallAndDie : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	public void PlayDeath(string clip)
    {

        Wriggle[] wriggles = GetComponentsInChildren<Wriggle>();

        foreach(Wriggle w in wriggles)
        {
            w.swaySpeed = 0.0f;
            Rigidbody2D r = w.GetComponent<Rigidbody2D>();
            if (r != null) r.gravityScale = 1.5f;
            Animator a = w.GetComponent<Animator>();
            if (a != null) a.Play(clip);
        }


    }


}
