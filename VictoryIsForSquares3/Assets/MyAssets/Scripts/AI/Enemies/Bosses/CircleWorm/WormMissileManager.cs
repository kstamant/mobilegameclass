﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormMissileManager : MonoBehaviour {

    public GameObject player;
    public PlayAnimation spittingAnim;

    public GameObject[] topRefs;
    public GameObject missilePrefab;

    public int numMissles = 20;

    public float spread = 1.0f;
    public float lag = 1.0f;

    public float delay = 1.0f;
    public float speed = 1.0f;
    public float speedScale = 1.0f;
    GameObject[] missiles;

    // Use this for initialization
    void Start () {

        missiles = new GameObject[numMissles];

        for (int j = 0; j < numMissles; ++j)
        {
            missiles[j] = Instantiate(missilePrefab, transform) as GameObject;
            missiles[j].GetComponent<WormMissile>().speed = speed + speedScale * j;

            missiles[j].SetActive(false);
        }

    }

    public IEnumerator MissileCoroutine()
    {
        Vector3 dir = ((player.transform.position + new Vector3(0, Random.Range(-spread, spread), 0)) -topRefs[0].transform.position).normalized;

        for (int j = 0; j < numMissles; ++j)
        {
            missiles[j].transform.SetParent(topRefs[0].transform);
            missiles[j].transform.localPosition = Vector2.zero;
            missiles[j].transform.SetParent(null);
            missiles[j].SetActive(true);
            Vector3 newDir = ((player.transform.position + new Vector3(0, Random.Range(-spread, spread), 0)) - missiles[j].transform.position).normalized;
            dir = Vector3.Lerp(dir, newDir, lag).normalized;

            missiles[j].SendMessage("Fire", dir, SendMessageOptions.DontRequireReceiver);
            if (j < numMissles - 1)
            {
                yield return new WaitForSeconds(delay);
            }
            else
            {
                yield return new WaitForSeconds(delay/2.0f);
            }
        }

        spittingAnim.PlayAnim("IdleSegment");
        yield break;
    }

    public void FireMissiles()
    {

        StartCoroutine(MissileCoroutine());
    }





    /*
      public GameObject player;

    public GameObject[] topRefs;
    public GameObject missilePrefab;
    public float delay = 1.0f;
    public float speed = 1.0f;
    public float speedScale = 1.0f;
    GameObject[] missiles;

    // Use this for initialization
    void Start () {

        missiles = new GameObject[topRefs.Length];

        for (int j = 0; j < missiles.Length; ++j)
        {
            missiles[j] = Instantiate(missilePrefab, transform) as GameObject;
            missiles[j].GetComponent<WormMissile>().speed = speed + speedScale * j;

            missiles[j].SetActive(false);
        }

    }

    public IEnumerator MissileCoroutine()
    {
        for (int j = 0; j < missiles.Length; ++j)
        {
            missiles[j].transform.SetParent(topRefs[0].transform);
            missiles[j].transform.localPosition = Vector2.zero;
            missiles[j].transform.SetParent(null);
            missiles[j].SetActive(true);
            missiles[j].SendMessage("Fire", (player.transform.position - missiles[j].transform.position).normalized, SendMessageOptions.DontRequireReceiver);
            yield return new WaitForSeconds(delay);
        }
        yield break;
    }

    public void FireMissiles()
    {

        StartCoroutine(MissileCoroutine());
    }

    // Update is called once per frame
    void Update () {
		
	}

    */




}
