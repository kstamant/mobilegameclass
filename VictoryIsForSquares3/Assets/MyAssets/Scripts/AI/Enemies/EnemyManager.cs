﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {

    public GameObject player;




	// Use this for initialization
	void Start () {
        EnemyHealth[] healthSystems = GetComponentsInChildren<EnemyHealth>();
        BasicCircleMovement[] moves = GetComponentsInChildren<BasicCircleMovement>();

        foreach (BasicCircleMovement b in moves)
        {
            
        }

        foreach (EnemyHealth h in healthSystems)
        {


            h.damageAnimEvent.AddListener(player.GetComponentInChildren<PlayerControl>().SmallJump);
            
        }

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
