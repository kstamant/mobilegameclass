﻿using UnityEngine;
using System.Collections;

public class MoveRight : MonoBehaviour {

    Rigidbody2D m_rigidbody;

    [SerializeField]
    private float m_speed = 1.0f;
	// Use this for initialization
	void Start () {
        m_rigidbody = GetComponent<Rigidbody2D>();

    }
	
	// Update is called once per frame
	void Update () {
        m_rigidbody.velocity = new Vector2(m_speed, 0.0f);

    }
}
