﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class InstantKill : MonoBehaviour {
    [SerializeField]
    private string m_damager = "Player";
    [SerializeField]
    private string m_deathAnimation = "Death";
     [SerializeField]
    private string m_deathReaction = "SmallJump";
    [SerializeField]
    private float m_pause = 0.1f;

    public void Kill()
    {
        GetComponent<Animator>().Play(m_deathAnimation);
        Destroy(gameObject, m_pause);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag(m_damager))
        {
            other.SendMessageUpwards(m_deathReaction);
            Kill();
        }
    }
}
