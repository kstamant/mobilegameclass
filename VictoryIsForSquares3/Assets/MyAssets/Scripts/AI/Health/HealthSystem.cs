﻿using UnityEngine;
using System.Collections;

public class HealthSystem : MonoBehaviour {

    //[SerializeField]
    //private int m_maxHealth = 1;

    [SerializeField]
    private int m_health = 1;

    [SerializeField]
    private bool m_alive = true;

    public void Damage(int amount)
    {
        m_health -= amount;
    } 

    public void Kill()
    {
        m_alive = false;
    }

    public void Resurrect(int healing = 0)
    {
        m_alive = true;
        m_health += healing;
    }

    public bool GetAlive()
    {
        return m_alive;
    }

    public void Heal(int amount)
    {
        m_health += amount;
    }

    public int GetHP()
    {
        return m_health;
    }

    public void SetHP(int amount)
    {
        m_health = amount;
    }

    public void SetMaxHP(int amount)
    {
       // m_maxHealth = amount;
    }
	
}
