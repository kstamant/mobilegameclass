﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillScript : MonoBehaviour {


	public void Kill(float time)
    {
        if(time == 0)
        {
            Destroy(gameObject);
        }
        else
        {
            Destroy(gameObject, time);
        }
    }

}
