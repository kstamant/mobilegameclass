﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CirclonJump : MonoBehaviour {

    public Rigidbody2D body;
    // Use this for initialization
    public void Jump(float f)
    {
        body.AddForce(new Vector2(0, f));

    }
	
}
