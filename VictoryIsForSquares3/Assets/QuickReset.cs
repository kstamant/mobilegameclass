﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;

public class QuickReset : MonoBehaviour {

    float timer = 0.0f;
    public float resetTime = 1.5f;
    public string level = "StartMenu";
	// Update is called once per frame
	void Update () {
		if(CrossPlatformInputManager.GetButton("Reset"))
        {
            timer += Time.deltaTime;
        }
        else
        {
            timer = 0.0f;
        }


        if(timer >= resetTime)
        {
            SceneManager.LoadScene(level);
        }
	}
}
