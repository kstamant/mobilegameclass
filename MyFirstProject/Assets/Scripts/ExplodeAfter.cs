﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeAfter : MonoBehaviour {
    [SerializeField]
    private GameObject obj;
    [SerializeField]
    private float lifeTime = 1.0f;
    private float time = 0.0f;
    bool enab = false;
    void OnEnable()
    {
        enab = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (enab)
        {
            time += Time.deltaTime;
            if (time >= lifeTime)
            {
                Instantiate(obj, transform.position, transform.rotation);
                Destroy(gameObject);
            }
        }
    }
}
