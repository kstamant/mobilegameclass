﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explode : MonoBehaviour {


    [SerializeField]
    private float force = 10.0f;
    [SerializeField]
    private float radius = 10.0f;

    [SerializeField]
    private float deathTime = 1.0f;
    void OnEnable()
    {
       foreach(Rigidbody r in GetComponentsInChildren<Rigidbody>())
        {
            r.AddExplosionForce(force, transform.position, radius);
        }
        DestroyObject(gameObject, deathTime);

    }
	
	
}
