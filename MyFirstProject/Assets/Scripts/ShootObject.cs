﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootObject : MonoBehaviour
{

    [SerializeField]
    private GameObject ammo;
    // Use this for initialization
    [SerializeField]
    private float speed;
    [SerializeField]
    private float lifeTime;
    [SerializeField]
    private float fireRate;

    private float timer = 0.0f;
    private void FixedUpdate()
    {
        
        if(Input.GetMouseButton(0))
        {
            timer += Time.deltaTime;
            if (timer >= fireRate)
            {
                GameObject shot = Instantiate(ammo, transform.position + transform.forward * 2.0f, Quaternion.identity) as GameObject;

                shot.GetComponent<Rigidbody>().AddForce(transform.forward * speed);
                timer = 0.0f;
            }
            //TravelInDirection tid = shot.GetComponent<TravelInDirection>();
            //  tid.Speed = speed;
            //  tid.Direction = transform.forward;
            //  tid.LifeTime = lifeTime;
        }
    }

}
