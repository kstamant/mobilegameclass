﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SimpleRotation : MonoBehaviour {

    [SerializeField]
    private float degPerSec = 90.0f;


    //Immediately when created
    void Awake()
    {

    }

    //When enabled
    void OnEnable()
    {

    }
    //When disabled
    void OnDisable()
    {

    }

    // Use this for initialization, after awake
    void Start ()
    {
		
	}

	

	// Update is called once per frame
	void Update ()
    {
        
        transform.Rotate(Vector3.right, degPerSec * Time.deltaTime);

	}

    //After update
    void LateUpdate()
    {

    }

    void FixedUpdate()
    {

    }

    void OnDestroy()
    {

    }

  

}
