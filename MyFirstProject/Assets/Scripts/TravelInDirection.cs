﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TravelInDirection : MonoBehaviour {
    [SerializeField]
    public float Speed;
    [SerializeField]
    public Vector3 Direction;
    [SerializeField]
    public float LifeTime;
    private float time = 0;
    // Use this for initialization

	
   private void Update()
    {
        time += Time.deltaTime;
        if(time >= LifeTime)
        {
            Destroy(gameObject);

        }
        else
        {
            transform.Translate(Direction * Speed * Time.deltaTime);
        }

    }
}
