﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLook : MonoBehaviour {

    [SerializeField]
    private float sensitivity = 2.0f;

    private float rotationAngle = 0.0f;

	// Use this for initialization
	void Start () {
		
	}

    void FixedUpdate()
    {
        if (Input.GetMouseButton(1))
        {
            rotationAngle -= Input.GetAxis("Mouse Y") * sensitivity;
            if (rotationAngle >= 90.0f) rotationAngle = 89.99f;
            else if (rotationAngle <= -90.0f) rotationAngle = -89.99f;
            transform.rotation = Quaternion.Euler(rotationAngle, transform.parent.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
        }


    }

}
