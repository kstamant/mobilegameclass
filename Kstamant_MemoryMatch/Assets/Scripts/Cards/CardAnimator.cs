﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardAnimator : MonoBehaviour {

    public float flipTime = 1.0f;
    public float time = 0.0f;
    public bool isFlipping = false;
    public bool infiniteFlip = false;
    public bool disableCallback = false;
    public CardInfo info;
    public Vector3 rotateAmt;

    void Awake()
    {
        info = GetComponent<CardInfo>();
    }

    public void Flip()
    {
        if(info.faceUp)
        {
            isFlipping = true;
            rotateAmt = new Vector3(0, 180.0f / flipTime , 0);
            info.faceUp = !info.faceUp;
        }
        else if (!isFlipping)
        {
            isFlipping = true;
            rotateAmt = new Vector3(0, -180.0f / flipTime,  0);
            info.faceUp = !info.faceUp;
        }

    }

	void Update () {
		
        if(isFlipping || infiniteFlip)
        {
            time += Time.deltaTime;
            if(time >= flipTime)
            {
                isFlipping = false;
                gameObject.transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, ((info.faceUp) ? 180.0f : 0.0f),  transform.rotation.eulerAngles.z);
                time = 0.0f;
            }
            else
            {
                gameObject.transform.Rotate(rotateAmt * Time.deltaTime);
            }

        }

	}

}
