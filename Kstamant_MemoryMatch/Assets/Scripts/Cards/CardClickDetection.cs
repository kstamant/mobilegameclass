﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardClickDetection : MonoBehaviour {

    CardAnimator card;
    //GameManager manager;
    void Start()
    {
        card = GetComponentInChildren<CardAnimator>();
    }

    void OnMouseDown()
    {
        if (!card.info.faceUp && !card.isFlipping)
        {
            Debug.Log("Clicked");
            SendMessageUpwards("RequestFlip", card);
        }
    }

}
