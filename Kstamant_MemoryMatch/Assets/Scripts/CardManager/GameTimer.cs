﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour {


    public Text timerText;

    public float endTime = 15.0f;
    public float time = 0.0f;
    public int displayTime = 0;
    public bool gameIsRunning = false;

    void Start () {
        time = endTime;

        DisplayTime();
    }

	void Update () {

        if (gameIsRunning)
        {
            time -= Time.deltaTime;


            if (time <= 0.0f)
            {
                gameIsRunning = false;
                time = 0.0f;
            }
            DisplayTime();

        }

	}


    void DisplayTime()
    {
        string result = Strings.timePrefix;
        float modVal = Mathf.Repeat(time, 1.0f);
        string seconds = (time - modVal).ToString();
        float ms = ((modVal * 10000.0f) / 60.0f);
        string milliseconds = (ms - Mathf.Repeat(ms, 1.0f)).ToString();
        
        result += seconds + "." + milliseconds;
        timerText.text = result;
    }

}
