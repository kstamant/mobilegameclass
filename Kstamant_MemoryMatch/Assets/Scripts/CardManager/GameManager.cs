﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GameTimer))]
public class GameManager : MonoBehaviour {


    public GameObject[] prefabCards;
    // Update is called once per frame
    GameObject[] gameCards;
    GameTimer timer;
    Timer t;
    public int selectedCards = 0;
    public int flippingCards = 0;

    public int numCards = 10;

    public float offsetX = .5f;
    public float offsetY = 0.0f;

    public float leftBounds = -5f;
    public float bottomBounds = -5f;

    public float stepX = 2.5f;
    public float stepY = 2.5f;

    public GameObject card1;
    public GameObject card2;

    public AudioSource pointAudio;
    public AudioSource loseAudio;

    public Text scoreText;
    public Text comboText;

    public Text winText;
    public Text loseText;


    public int matches = 0;
    public int score = 0;
    public int combo = 0;


    // private bool isStarted = false;
    public float startTime = 2.0f;
   // private float time = 0.0f;

    

    void Start()
    {
       

        scoreText.text = "Score: 0";
        comboText.text = "Combo: 0";
        timer = GetComponent<GameTimer>();
        if (numCards % 2 == 1) numCards++;
        float posX = leftBounds;
        float posY = bottomBounds;

        gameCards = new GameObject[numCards];

        ArrayList positions = new ArrayList();
      
        for (int j = 0; j < numCards; j++)
        {
            
            positions.Add(new Vector3(posX + offsetX, posY + offsetY,-1));
            posX += stepX;
            if (posX >= -leftBounds)
            {
                posY += stepY;
                posX = leftBounds;
            }
        }

        int len = positions.Count - 1;
        int ct = 0;

        for (int j = 0; j < numCards; j += 2)
        {
            int v = Random.Range(0, len);
            Vector3 temp = (Vector3) positions[v];
            gameCards[j] = Instantiate(prefabCards[ct], temp, Quaternion.identity, this.transform) as GameObject;
            positions.RemoveAt(v);
            len = positions.Count - 1;

            v = Random.Range(0, len);
            temp = (Vector3)positions[v];
            gameCards[j+1] = Instantiate(prefabCards[ct], temp, Quaternion.identity, this.transform) as GameObject;
            positions.RemoveAt(v);
            len = positions.Count - 1;

            ct++;
            if(ct >= prefabCards.Length)
            {
                ct = 0;
            }
        }

        FlipAll();
        Invoke("StartMatchingGame", startTime);

    }
    
   // private void StartMatchingGame(object sender, ElapsedEventArgs e)
   // {
   //     t.Stop();
   //     t.Elapsed -= StartMatchingGame;
   //     FlipAll();
   // }

    void StartMatchingGame()
    {
        FlipAll();
        timer.gameIsRunning = true;
    }

    void FlipAll()
    {
        for (int j = 0; j < numCards; j ++)
        {
            gameCards[j].GetComponentInChildren<CardAnimator>().Flip();
        }
    }


    public void RequestFlip(CardAnimator card)
    {
        if (timer.gameIsRunning)
        {
            if (selectedCards == 0)
            {
                selectedCards++;
                card.SendMessage("Flip");
                card1 = card.gameObject.transform.parent.gameObject;
            }
            else if (selectedCards == 1)
            {
                selectedCards++;
                card.SendMessage("Flip");
                card2 = card.gameObject.transform.parent.gameObject;
                this.Invoke("CheckMatch", card2.GetComponentInChildren<CardAnimator>().flipTime);
               // CheckMatch();
                //CheckMatch
            }
            //else
            //{
            //    //Play bad sound
            //}
        }
    }

    public void CheckMatch()
    {

        if (card1.GetComponentInChildren<CardInfo>().id == card2.GetComponentInChildren<CardInfo>().id)
        {

            Destroy(card1.gameObject, 1.0f);
            Destroy(card2.gameObject, 1.0f);

            //  card1.gameObject.transform.position = new Vector3(-100.0f, 0.0f, 0.0f);
            //  card2.gameObject.transform.position = new Vector3(-100.0f, 0.0f, 0.0f);

            card1.GetComponentInChildren<CardAnimator>().infiniteFlip = true;
            card2.GetComponentInChildren<CardAnimator>().infiniteFlip = true;
            card1.GetComponent<Collider2D>().enabled = false;
            card2.GetComponent<Collider2D>().enabled = false;
            card1.GetComponentInChildren<CardAnimator>().flipTime = .05f;
            card2.GetComponentInChildren<CardAnimator>().flipTime = .05f;
            card1.GetComponentInChildren<CardAnimator>().SendMessage("Flip");
            card2.GetComponentInChildren<CardAnimator>().SendMessage("Flip");

            pointAudio.Play();
            matches++;
            score += combo + 1;
            scoreText.text = Strings.scorePrefix + score.ToString();
            combo++;
            comboText.text = Strings.comboPrefix + combo.ToString();
            selectedCards = 0;
            card1 = null;
            card2 = null;

            if(matches * 2 == numCards)
            {
                winText.gameObject.SetActive(true);
                timer.gameIsRunning = false;
            }


        }
        else
        {
            combo = 0;
            comboText.text = Strings.comboPrefix + combo.ToString();
            card1.GetComponentInChildren<CardAnimator>().SendMessage("Flip");
            card2.GetComponentInChildren<CardAnimator>().SendMessage("Flip");

            selectedCards = 0;
            card1 = null;
            card2 = null;
        }
    }

    void Update()
    {

        if(!loseText.gameObject.activeInHierarchy && timer.time == 0.0f)
        {
            loseText.gameObject.SetActive(true);
            loseAudio.Play();
        }
    }




}
