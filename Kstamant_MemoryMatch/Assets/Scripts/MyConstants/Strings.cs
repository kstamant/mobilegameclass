﻿using System;



public static class Strings
{
    public const string clicked = "Clicked";
    public const string scorePrefix = "Score: ";
    public const string comboPrefix = "Combo: ";
    public const string timePrefix = "Time: ";

}
