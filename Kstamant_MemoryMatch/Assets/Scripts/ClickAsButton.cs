﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class ClickAsButton : MonoBehaviour {

    Animator myAnimator;
	// Use this for initialization
	void Start () {
        myAnimator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	public void DoSomething()
    {
        Debug.Log("Oh no");
        myAnimator.SetTrigger(Strings.clicked);
    }
}
